#!/bin/bash
# A simple shell script to install packages, and maybe addition configuration.
# Project URL: https://gitlab.com/ubuntu1/desktop-setup/
# Tested on : Ubuntu 16.04 LTS lxd server
# ----------------------------------------------------------------------------
# Author: Dale Noe
# Copyright: 2020 Dale Noe (dalenoe@gmail.com)
# ----------------------------------------------------------------------------
# Last updated 22 Feb 2020
# ----------------------------------------------------------------------------

# List of packages to install in a single line with spaces
_packages="htop bmon iotop speedtest fail2ban"

# A little echo
echo "Installing the following packages... "

# Install packages
sudo apt install $_packages
