# Ubuntu Computer Setup

This repository contains a couple bash scripts that will be used to install
packages that I need. Eventually I would like to make it more involved. Setting
up various environments and such.

It will currently install the following, there is also a [issue open](https://gitlab.com/ubuntu1/ubuntu-computer-setup/-/issues/1)

## Packages

- htop
- bmon
- iotop
- speedtest
- fail2ban

### TODO

- [ ] Google Chrome (not in the software center)
- [ ] VLC
- [ ] More To Be Listed!
